module.exports = {
  future: {
    // removeDeprecatedGapUtilities: true,
    // purgeLayersByDefault: true,
  },
  purge: [],
  theme: {
    backgroundColor: theme=> ({
      'nav': '#0099ff',
      'orange-500': '#ed8936',
      'black-nav': '#1a1a1a',
      'gray1': '#f2f2f2',
    }),
    extend: {
      fontFamily: {
        Pop:['Poppins']
      },
    },
  },
  variants: {},
  plugins: [],
}
